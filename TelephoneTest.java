import static org.junit.jupiter.api.Assertions.*;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class TelephoneTest {

	@Test
	void testOne() {
		String test = Telephone.unformat("(717)444-5555");
		Assert.assertEquals("7174445555", test);
	}
	
	@Test
	void testTwo() {
		String test = Telephone.unformat("(111)111-1111");
		Assert.assertEquals("1111111111", test);
	}
	
	@Test
	void testThree() {
		String test = Telephone.unformat("123-456-7777");
		Assert.assertEquals("1234567777", test);
	}

}
